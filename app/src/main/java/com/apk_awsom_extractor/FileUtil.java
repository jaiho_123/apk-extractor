package com.apk_awsom_extractor;

import java.io.File;

public class FileUtil {
    public static String getFileType(File f) {
        String end = f.getName().substring(f.getName().lastIndexOf(".") + 1, f.getName().length()).toLowerCase();
        if (end == null || !end.equalsIgnoreCase("apk")) {
            return BuildConfig.VERSION_NAME;
        }
        return "apk";
    }

    public static String getFileSize(long bytes, boolean si) {
        if (si) {
        }
        if (bytes < ((long) 1024)) {
            return bytes + "B";
        }
        String pre = (si ? "KMGTPE" : "KMGTPE").charAt(((int) (Math.log((double) bytes) / Math.log((double) 1024))) - 1) + (si ? BuildConfig.VERSION_NAME : BuildConfig.VERSION_NAME);
        return String.format("%.1f %sB", new Object[]{Double.valueOf(((double) bytes) / Math.pow((double) 1024, (double) ((int) (Math.log((double) bytes) / Math.log((double) 1024))))), pre});
    }
}
