package com.apk_awsom_extractor;

import android.graphics.drawable.Drawable;
import java.util.Comparator;

public class ApkFile implements Comparator<ApkFile> {
    Drawable appIcon;
    String appName;
    String fileName;
    String filePath;
    long fileSize;

    public ApkFile(String fileName, String filePath, long fileSize, String appName, Drawable appIcon) {
        this.fileName = fileName;
        this.filePath = filePath;
        this.fileSize = fileSize;
        this.appName = appName;
        this.appIcon = appIcon;
    }

    public int compare(ApkFile object1, ApkFile object2) {
        return object1.appName.compareTo(object2.appName);
    }

    public String getFileSize() {
        return FileUtil.getFileSize(this.fileSize, true);
    }
}
