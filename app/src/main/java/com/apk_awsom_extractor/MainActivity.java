package com.apk_awsom_extractor;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity implements BackupListener {
    public static final String AD_UNIT_ID = "ca-app-pub-5374808815187373/2949005449";
    //   private AdView adView;
    private LinearLayout ad_layout;
    private ApkAdapter apk_adapter;
    public List<ApkFile> apk_list = new ArrayList();
    private BackupAdapter backup_adapter;
    public List<AppInfo> backup_list = new ArrayList();
    private Button button_backup;
    private Button button_installer;
    private Handler handler = new Handler();
    private boolean isLoading = false;
    private ListView list_view;
    private OnClickListener onInstall = new OnClickListener() {
        public void onClick(View v) {
            MainActivity.this.installAPK(v.getId());
        }
    };
    private OnClickListener onViewClick = new OnClickListener() {
        public void onClick(View v) {
            final int position = v.getId();
            CharSequence[] items = new CharSequence[]{"Install", "Share", "Delete"};
            Builder builder = new Builder(MainActivity.this);
            builder.setItems(items, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    switch (item) {
                        case 0 /*0*/:
                            MainActivity.this.installAPK(position);
                            break;
                        case 1 /*1*/:
                            MainActivity.this.share(position);
                            break;
                        case 2 /*2*/:
                            MainActivity.this.deleteAPK(position);
                            break;
                    }
                    dialog.dismiss();
                }
            });
            builder.create().show();
        }
    };
    private LinearLayout progress_layout;
    private TextView text_view;

    class ApkAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public ApkAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return MainActivity.this.apk_list.size();
        }

        public Object getItem(int position) {
            return MainActivity.this.apk_list.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = this.mInflater.inflate(R.layout.list_item1, null);
                holder = new ViewHolder();
                holder.app_icon = (ImageView) convertView.findViewById(R.id.app_icon);
                holder.app_name = (TextView) convertView.findViewById(R.id.app_name);
                holder.app_size = (TextView) convertView.findViewById(R.id.app_size);
                holder.install_btn = (Button) convertView.findViewById(R.id.install_button);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            ApkFile apk = (ApkFile) MainActivity.this.apk_list.get(position);
            if (apk.appIcon != null) {
                holder.app_icon.setImageDrawable(apk.appIcon);
            } else {
                holder.app_icon.setImageResource(R.drawable.ic_launcher);
            }
            if (apk.appName != null) {
                holder.app_name.setText(apk.appName);
            } else {
                holder.app_name.setText(apk.fileName);
            }
            holder.app_size.setText(apk.getFileSize());
            holder.install_btn.setId(position);
            holder.install_btn.setOnClickListener(MainActivity.this.onInstall);
            convertView.setId(position);
            convertView.setOnClickListener(MainActivity.this.onViewClick);
            return convertView;
        }
    }

    static class ViewHolder {
        ImageView app_icon;
        TextView app_name;
        TextView app_size;
        Button install_btn;

        ViewHolder() {
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        this.button_installer = (Button) findViewById(R.id.button_installer);
        this.button_installer.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (MainActivity.this.isLoading) {
                    MainActivity.this.showToast("Please Wait Loading...");
                    return;
                }
                button_installer.setBackgroundColor(Color.parseColor("#420f6e"));
                button_backup.setBackgroundColor(Color.parseColor("#9a0000"));
                apk_adapter = new ApkAdapter(MainActivity.this);
                list_view.setAdapter(MainActivity.this.apk_adapter);
                showProgress();
                showStatus("Searching APK Files From Memory...");
                clearAPKAdapter();
                scanFileSystem();
            }
        });
        this.button_backup = (Button) findViewById(R.id.button_backup);
        this.button_backup.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (MainActivity.this.isLoading) {
                    MainActivity.this.showToast("Please Wait Loading...");
                    return;
                }
                button_installer.setBackgroundColor(Color.parseColor("#9a0000"));
                button_backup.setBackgroundColor(Color.parseColor("#420f6e"));
                backup_adapter = new BackupAdapter(MainActivity.this, MainActivity.this);
                list_view.setAdapter(MainActivity.this.backup_adapter);
                showProgress();
                showStatus("Loading Applications...");
                clearBackupAdapter();
                loadApplications();
            }
        });
        this.progress_layout = (LinearLayout) findViewById(R.id.progress_layout);
        this.text_view = (TextView) findViewById(R.id.status);
        this.list_view = (ListView) findViewById(R.id.list_view);
        //   this.adView = new AdView(this);
        //   this.adView.setAdSize(AdSize.BANNER);
        //   this.adView.setAdUnitId(AD_UNIT_ID);
        this.ad_layout = (LinearLayout) findViewById(R.id.adlayout);
        //  this.ad_layout.addView(this.adView);
        //   this.adView.loadAd(new AdRequest.Builder().build());
        //   this.button_installer.performClick();
    }

    private void loadApplications() {
        new Thread(new Runnable() {
            public void run() {
                MainActivity.this.backup_adapter.getFiles();
                MainActivity.this.getDownloadedApps();
                MainActivity.this.hideProgress();
            }
        }).start();
    }

    private void getDownloadedApps() {
        PackageManager pkg = getPackageManager();
        Intent mainIntent = new Intent("android.intent.action.MAIN", null);
        mainIntent.addCategory("android.intent.category.LAUNCHER");
        List<ResolveInfo> mApps = pkg.queryIntentActivities(mainIntent, 0);
        for (int i = 0; i < mApps.size(); i++) {
            AppInfo app = new AppInfo();
            ResolveInfo info = (ResolveInfo) mApps.get(i);
            app.AppName = (String) info.loadLabel(pkg);
            app.PackageName = info.activityInfo.packageName;
            app.Path = info.activityInfo.applicationInfo.publicSourceDir;
            try {
                app.AppIcon = pkg.getApplicationIcon(app.PackageName);
                app.Size = FileUtil.getFileSize(new File(app.Path).length(), true);
            } catch (Exception e) {
                e.printStackTrace();
            }
            onAppLoad(app);
        }
    }

    private void onAppLoad(final AppInfo app) {
        runOnUiThread(new Runnable() {
            public void run() {
                if (MainActivity.this.backup_adapter != null) {
                    MainActivity.this.backup_adapter.putAppInfo(app);
                    MainActivity.this.backup_adapter.notifyDataSetChanged();
                }
            }
        });
    }

    private void showProgress() {
        this.handler.post(new Runnable() {
            public void run() {
                MainActivity.this.progress_layout.setVisibility(View.VISIBLE);
            }
        });
    }

    private void hideProgress() {
        this.handler.post(new Runnable() {
            public void run() {
                MainActivity.this.progress_layout.setVisibility(View.GONE);
            }
        });
    }

    private void showStatus(final String message) {
        this.handler.post(new Runnable() {
            public void run() {
                MainActivity.this.text_view.setVisibility(View.VISIBLE);
                MainActivity.this.text_view.setText(message);
            }
        });
    }

    private void hideStatus() {
        this.handler.post(new Runnable() {
            public void run() {
                MainActivity.this.text_view.setVisibility(View.GONE);
            }
        });
    }

    public void onSuccess() {
        runOnUiThread(new Runnable() {
            public void run() {
                MainActivity.this.showToast("Backup Success!!!");
                if (MainActivity.this.backup_adapter != null) {
                    MainActivity.this.backup_adapter.notifyDataSetChanged();
                }
            }
        });
    }

    public void onFailure() {
        showToast("Backup Failure!!!");
    }

    private void deleteAPK(int position) {
        File file = new File(((ApkFile) this.apk_list.get(position)).filePath);
        if (file != null && file.delete()) {
            this.apk_list.remove(position);
            showToast("APK File Deleted!");
            this.apk_adapter.notifyDataSetChanged();
        }
    }

    private void installAPK(int position) {
        try {
            File file = new File(((ApkFile) this.apk_list.get(position)).filePath);
            Intent install = new Intent("android.intent.action.VIEW");
            install.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
            startActivity(install);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void share(int position) {
        try {
            File file = new File(((ApkFile) this.apk_list.get(position)).filePath);
            Intent sharingIntent = new Intent("android.intent.action.SEND");
            sharingIntent.addCategory("android.intent.category.DEFAULT");
            sharingIntent.setType("application/vnd.android.package-archive");
            sharingIntent.putExtra("android.intent.extra.SUBJECT", "Sharing Application From APK Installer");
            sharingIntent.putExtra("android.intent.extra.TEXT", "Available on Play Store https://play.google.com/store/apps/details?id=com.dalvik.apkinstaller&hl=en");
            sharingIntent.putExtra("android.intent.extra.STREAM", Uri.fromFile(file));
            startActivity(Intent.createChooser(sharingIntent, "Share to Friends"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showToast(final String msg) {
        runOnUiThread(new Runnable() {
            public void run() {
                Toast toast = Toast.makeText(getApplication(), msg, Toast.LENGTH_SHORT);
                View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.toast_custom, null);
                ((TextView) view.findViewById(R.id.icon_text)).setText(msg);
                toast.setView(view);
                toast.show();
            }
        });
    }

    private boolean isMediaPresent() {
        if ("mounted".equals(Environment.getExternalStorageState())) {
            return true;
        }
        return false;
    }

    private void scanFileSystem() {
        new Thread(new Runnable() {
            public void run() {
                MainActivity.this.isLoading = true;
                if (MainActivity.this.isMediaPresent()) {
                    File root = Environment.getExternalStorageDirectory();
                    if (root != null) {
                        MainActivity.this.getApkFileList(root);
                    }
                    String path = System.getenv("SECONDARY_STORAGE");
                    if (path != null) {
                        File sdcard = new File(path);
                        if (sdcard != null) {
                            MainActivity.this.getApkFileList(sdcard);
                        }
                    }
                    if (MainActivity.this.apk_list.isEmpty()) {
                        MainActivity.this.showStatus("No APK Files Found!!!");
                        MainActivity.this.showToast("No APK Files Found!!!");
                    }
                } else {
                    MainActivity.this.showToast("No Memory Card Found!!!");
                }
                MainActivity.this.hideProgress();
                MainActivity.this.isLoading = false;
            }
        }).start();
    }

    private void clearAPKAdapter() {
        this.apk_list.clear();
        this.list_view.setAdapter(this.apk_adapter);
    }

    private void clearBackupAdapter() {
        this.backup_list.clear();
        this.list_view.setAdapter(this.backup_adapter);
    }

    public void getApkFileList(File root) {
        if (root != null && root.isDirectory()) {
            File[] files = root.listFiles();
            if (files != null) {
                for (File file : files) {
                    if (!file.isFile()) {
                        getApkFileList(file);
                    } else if (FileUtil.getFileType(file).equalsIgnoreCase("apk")) {
                        onApkFileFound(file);
                    }
                }
            }
        }
    }

    private void onApkFileFound(File file) {
        String fileName = file.getName();
        String filePath = file.getAbsolutePath();
        long fileSize = file.length();
        String appName = null;
        Drawable appIcon = null;
        PackageInfo packageInfo = getPackageManager().getPackageArchiveInfo(filePath, 0);
        if (packageInfo != null) {
            ApplicationInfo appInfo = packageInfo.applicationInfo;
            if (VERSION.SDK_INT >= 8) {
                appInfo.sourceDir = filePath;
                appInfo.publicSourceDir = filePath;
            }
            appName = (String) getPackageManager().getApplicationLabel(appInfo);
            appIcon = getPackageManager().getApplicationIcon(appInfo);
        }
        refreshListView(new ApkFile(fileName, filePath, fileSize, appName, appIcon));
    }

    private void refreshListView(final ApkFile apkFile) {
        this.handler.post(new Runnable() {
            public void run() {
                MainActivity.this.apk_list.add(apkFile);
                MainActivity.this.apk_adapter.notifyDataSetChanged();
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.isLoading) {
            showToast("Searching APK Files");
            return false;
        }
        showExitDialog();
        return false;
    }

    private void showExitDialog() {
        Builder builder = new Builder(this);
        builder.setMessage("Do you want to exit?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                MainActivity.this.finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    protected void onDestroy() {
        super.onDestroy();
      /*  if (this.adView != null) {
            this.adView.destroy();
        }*/
    }
}
