package com.apk_awsom_extractor;

public interface BackupListener {
    void onFailure();

    void onSuccess();
}
