package com.apk_awsom_extractor;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class BackupAdapter extends BaseAdapter {
    private ArrayList<AppInfo> appList = new ArrayList();
    private Activity context;
    private ProgressDialog dialog;
    public File[] files;
    private BackupListener listener;
    private LayoutInflater mInflater;
    private OnClickListener onBackUp = new OnClickListener() {
        public void onClick(View v) {
            AppInfo info = (AppInfo) BackupAdapter.this.appList.get(v.getId());
            File apk_file = new File(info.Path);
            if (apk_file.exists()) {
                BackupAdapter.this.dialog = ProgressDialog.show(BackupAdapter.this.context, BuildConfig.VERSION_NAME, "Taking Backup...", true);
                BackupAdapter.this.dialog.setCancelable(false);
                BackupAdapter.this.doBackup(apk_file, info.AppName);
            } else if (BackupAdapter.this.listener != null) {
                BackupAdapter.this.listener.onFailure();
            }
        }
    };

    static class ViewHolder {
        ImageView app_icon;
        TextView app_name;
        TextView app_size;
        Button backup_btn;

        ViewHolder() {
        }
    }

    public BackupAdapter(Activity context, BackupListener listener) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        this.listener = listener;
    }

    public void putAppInfo(AppInfo app) {
        this.appList.add(app);
    }

    public int getCount() {
        return this.appList.size();
    }

    public Object getItem(int position) {
        return this.appList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = this.mInflater.inflate(R.layout.list_item2, null);
            holder = new ViewHolder();
            holder.app_icon = (ImageView) convertView.findViewById(R.id.app_icon);
            holder.app_name = (TextView) convertView.findViewById(R.id.app_name);
            holder.app_size = (TextView) convertView.findViewById(R.id.app_size);
            holder.backup_btn = (Button) convertView.findViewById(R.id.backup_button);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        AppInfo app = (AppInfo) this.appList.get(position);
        if (app.AppIcon != null) {
            holder.app_icon.setImageDrawable(app.AppIcon);
        }
        holder.app_name.setText(app.AppName);
        String str = BuildConfig.VERSION_NAME;
        app.isBackedUp = isAvailable(app.AppName + ".apk");
        int text_color = -11513776;
        if (app.isBackedUp) {
            str = "(On SDCard)";
            text_color = -65536;
        }
        holder.app_size.setText(app.Size + " " + str);
        holder.app_size.setTextColor(text_color);
        holder.backup_btn.setId(position);
        holder.backup_btn.setOnClickListener(this.onBackUp);
        return convertView;
    }

    private void doBackup(final File apkFile, final String appName) {
        new Thread(new Runnable() {
            public void run() {
                if (BackupAdapter.this.copyFile(apkFile, appName) != null) {
                    BackupAdapter.this.dismissDialog();
                    BackupAdapter.this.getFiles();
                    if (BackupAdapter.this.listener != null) {
                        BackupAdapter.this.listener.onSuccess();
                        return;
                    }
                    return;
                }
                BackupAdapter.this.dismissDialog();
                if (BackupAdapter.this.listener != null) {
                    BackupAdapter.this.listener.onFailure();
                }
            }
        }).start();
    }

    private void showToast(final String msg) {
        if (this.context != null) {
            this.context.runOnUiThread(new Runnable() {
                public void run() {
                    Toast toast = Toast.makeText(BackupAdapter.this.context, msg, 0);
                    View view = LayoutInflater.from(BackupAdapter.this.context).inflate(R.layout.toast_custom, null);
                    ((TextView) view.findViewById(R.id.icon_text)).setText(msg);
                    toast.setView(view);
                    toast.show();
                }
            });
        }
    }

    public void getFiles() {
        try {
            File dir = new File(Environment.getExternalStorageDirectory(), "AppBackup");
            if (dir.exists()) {
                this.files = dir.listFiles();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean isAvailable(String name) {
        boolean flag = false;
        try {
            for (File file : this.files) {
                if (file.getName().equalsIgnoreCase(name)) {
                    flag = true;
                }
            }
            return flag;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    private void dismissDialog() {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
    }

    private boolean isMediaPresent() {
        if ("mounted".equals(Environment.getExternalStorageState())) {
            return true;
        }
        return false;
    }

    private File copyFile(File src, String name) {
        File dir = createDir("AppBackup");
        if (dir == null) {
            return dir;
        }
        File dst = new File(dir, name + ".apk");
        try {
            copyFile(src, dst);
            return dst;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    void copyFile(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);
        byte[] buf = new byte[1024];
        while (true) {
            int len = in.read(buf);
            if (len > 0) {
                out.write(buf, 0, len);
            } else {
                in.close();
                out.close();
                return;
            }
        }
    }

    private File createDir(String name) {
        if (!isMediaPresent()) {
            return null;
        }
        File root = Environment.getExternalStorageDirectory();
        if (root == null) {
            return null;
        }
        File dir = new File(root, name);
        if (!(dir == null || dir.exists())) {
            dir.mkdir();
        }
        return dir;
    }
}
