package com.apk_awsom_extractor;

import android.graphics.drawable.Drawable;
import java.util.Comparator;

public class AppInfo implements Comparator<AppInfo> {
    public Drawable AppIcon;
    public String AppName;
    public String PackageName;
    public String Path;
    public String Size;
    public boolean isBackedUp;

    public int compare(AppInfo object1, AppInfo object2) {
        return object1.AppName.compareTo(object2.AppName);
    }
}
